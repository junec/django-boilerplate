#!/bin/bash

source $(cd $(dirname "$0"); pwd)/config/settings.conf

echo "Fetching latest changes..."
git fetch origin

echo "Stashing unsaved changes..."
git stash save

echo "Merging latest changes to master..."
git rebase origin/master

source $(cd $(dirname "$0"); pwd)/prepare.sh

echo "Applying stashed chages..."
git stash apply
