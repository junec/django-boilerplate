#!/bin/bash

source $(cd $(dirname "$0"); pwd)/config/settings.conf

echo "Checking logs directory..."
test -d $LOG_DIR || mkdir -p $LOG_DIR

echo "Checking executable binaries directory..."
test -d $BIN_DIR || mkdir -p $BIN_DIR

echo "Checking backup directory..."
test -d $BACKUP_DIR || mkdir -p $BACKUP_DIR

echo "Checking static root directory..."
test -d $STATIC_DIR || mkdir -p $STATIC_DIR

echo "Checking media root directory..."
test -d $MEDIA_DIR || mkdir -p $MEDIA_DIR

echo "Checking bower components root directory..."
test -d $BOWER_DIR || mkdir -p $BOWER_DIR

source $(cd $(dirname "$0"); pwd)/prepare.sh
