{
    "always_show_minimap_viewport": true,
    "bold_folder_labels": true,
    "detect_indentation": false,
    "draw_minimap_border": true,
    "draw_white_space": "all",
    "ensure_newline_at_eof_on_save": true,
    "fade_fold_buttons": false,
    "find_selected_text": true,
    "font_face": "Liberation Mono",
    "font_size": 8,
    "highlight_line": true,
    "ignored_packages": [],
    "line_padding_bottom": 1,
    "line_padding_top": 1,
    "margin": -1,
    "open_files_in_new_window": false,
    "rulers":
    [
        100,
        80
    ],
    "scroll_past_end": true,
    "shift_tab_unindent": true,
    "show_full_path": true,
    "tab_size": 4,
    "translate_tabs_to_spaces": true,
    "trim_automatic_white_space": false,
    "trim_trailing_white_space_on_save": false,
    "word_separators": "./\\()\"'-:,.;<>~!@#$%^&*|+=[]{}`~?"
}
