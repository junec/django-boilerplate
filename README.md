Django Boilerplate
==================

A base template for Django (v1.8.3) projects.

Suggested Project Creation
---------------------------
    
    0. create fork of bitbucket.org/njncalub/django-boilerplate.git
    
    1. update django-boilerplate to be used on your own projects
    
    2. `mkdir -p ~/projects/client_name/project_name && cd $_`
    
    3. `git clone git@bitbucket.org:username/django-boilerplate-fork.git src`

Setup:
------
    
    1. `cp ./bin/config/settings.conf.tpl ./bin/config/settings.conf`
    
    2. update settings.conf `PROJECT_NAME`
    
    3. update settings.conf `PROJECT_LEVEL` to [ 'base', 'devt', 'test', 'prod' ]
    
    4. `./bin/init.sh`

Bash Scripts
------------
    
    0. ./bin/init.sh
        add required project files (sublime, local_settings, etc) for editing
    
    1. ./bin/setup.sh
        setup required project directories
    
    2. ./bin/prepare.sh
        prepare local machine
    
    3. ./bin/sync.sh
        sync repo with the origin/master

Contribution
------------
    
    Feel free to issue pull requests guys! :)
    This repository is still very much a work in progress and may not work properly for some people. If you find any issues, don't hesitate to file an issue or submit a pull request with your fix.
